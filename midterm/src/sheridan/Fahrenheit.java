package sheridan;

public class Fahrenheit {
	
	public static int fromCelsius(int temp) {
		
		double fahreheitDouble = (9.0/5) * temp + 32;
		
		int fahrenheitInt = (int) Math.round(fahreheitDouble);
		
		return fahrenheitInt;
	}
}
