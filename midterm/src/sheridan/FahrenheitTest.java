package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Samandeep Singh
 *
 */
public class FahrenheitTest {

	@Test
	public void testFromCelciusRegular() {
		int result = Fahrenheit.fromCelsius(25);
		assertTrue("Invalid temperature value returned", result == 77);
	}
	
	@Test
	public void testFromCelciusException() {
		int result = Fahrenheit.fromCelsius(-150);
		assertTrue("Invalid temperature value returned", result == -238);
	}
	
	@Test
	public void testFromCelciusBoundaryIn() {
		int result = Fahrenheit.fromCelsius(0);
		assertTrue("Invalid temperature value returned", result == 32);
	}
	
	@Test
	public void testFromCelciusBoundaryOut() {
		int result = Fahrenheit.fromCelsius(-1);
		assertTrue("Invalid temperature value returned", result == 30);
	}

}
